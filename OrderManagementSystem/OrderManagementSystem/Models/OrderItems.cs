﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderManagementSystem.Models
{
	public class OrderItems
	{
		public int ItemId { get; set; }
		public int OrderId { get; set; }
		public int ProductId { get; set; }
		public int Quantity { get; set; }
		public bool isActive { get; set; }
		public string ProductName { get; set; }

	}
}