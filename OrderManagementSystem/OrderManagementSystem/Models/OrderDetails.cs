﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderManagementSystem.Models
{
	public class OrderDetails
	{
		public OrderDetails() {
			Orders = new List<OrderItems>();
		}

		public int OrderId { get; set; }
		public int BuyerId { get; set; }
		public string BuyerName { get; set; }
		public int AddressId { get; set; }
		public string DeliveryAddress { get; set; }
		public string PrimaryContactNo { get; set; }
		public int OrderStatus { get; set; }
		public string OrderStatusDesc { get; set; }
		public string OrderPlacedOn { get; set; }
		public List<OrderItems> Orders { get; set; }
	}

	public class OrderUpdate
	{
		public int OrderId { get; set; }
		public int OrderStatus { get; set; }
	}
}