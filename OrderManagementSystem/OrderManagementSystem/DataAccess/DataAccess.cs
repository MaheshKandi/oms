﻿// Created By :   Mahesh Kandi
// Created Date : 31st Dec, 2020
// Description :  This file contains the implementations of the Data access layer.
// ==============================================================================


using log4net;
using OrderManagementSystem.Models;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace OrderManagementSystem.SQLDataAccess
{
	public class DataAccess
	{
		ILog _log = log4net.LogManager.GetLogger(typeof(DataAccess));
		public static readonly string _conStrOMS = Convert.ToString(ConfigurationManager.ConnectionStrings["conStrOMS"]);

		public int InsertOrderDetails(int BuyerId, int AddressId, int OrderStatus)
		{
			try
			{
				// Create the SQL connection.
				SqlConnection mConnection = new SqlConnection(_conStrOMS);

				// Create the SQL command.
				string commandString = "InsertOrderDetails";
				SqlCommand command = new SqlCommand(commandString, mConnection);
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.AddWithValue("@BuyerId", BuyerId);
				command.Parameters.AddWithValue("@AddressId", AddressId);
				command.Parameters.AddWithValue("@OrderStatus", OrderStatus);

				SqlParameter outputPara = new SqlParameter();
				outputPara.ParameterName = "@OrderId";
				outputPara.Direction = System.Data.ParameterDirection.Output;
				outputPara.SqlDbType = System.Data.SqlDbType.Int;
				command.Parameters.Add(outputPara);

				// Execute the command.
				mConnection.Open();
				command.ExecuteNonQuery();

				// Close the connection.
				mConnection.Close();

				return Convert.ToInt32(outputPara.Value);
			}
			catch(Exception ex)
			{
				_log.Error("Error while inserting the order details. Details : " + ex.Message);
				throw ex;
			}
		}

		public void InsertOrderItemDetails(int OrderId, int ProductId, int Quantity)
		{
			try
			{
				// Create the SQL connection.
				SqlConnection mConnection = new SqlConnection(_conStrOMS);

				// Create the SQL command.
				string commandString = "InsertOrderItems";
				SqlCommand command = new SqlCommand(commandString, mConnection);
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.AddWithValue("@OrderId", OrderId);
				command.Parameters.AddWithValue("@ProductId", ProductId);
				command.Parameters.AddWithValue("@Quantity", Quantity);

				// Execute the command.
				mConnection.Open();
				command.ExecuteNonQuery();

				// Close the connection.
				mConnection.Close();
			}
			catch(Exception ex)
			{
				_log.Error("Error while inserting the order item details. Details : " + ex.Message);
				throw ex;
			}
		}

		public bool UpdateOrderStatusByOrderId(int OrderId, int StatusId)
		{
			try
			{
				SqlConnection mConnection = new SqlConnection(_conStrOMS);

				string commandString = "UpdateOrderStatusByOrderId";

				SqlParameter[] sqlParams = {
				 new SqlParameter("@OrderId", OrderId),
				 new SqlParameter("@OrderStatus", StatusId)
				 };

				SqlHelper.ExecuteNonQuery(mConnection, commandString, sqlParams);
				return true;
			}
			catch(Exception ex)
			{
				_log.Error("Error while updating the order details for the Order id : " + OrderId +". Details : " + ex.Message);
				throw ex;
			}
		}

		public void UpdateOrderItemDetails(OrderItems order, int OrderId)
		{
			try
			{
				// Create the SQL connection.
				SqlConnection mConnection = new SqlConnection(_conStrOMS);

				// Create the SQL command.
				string commandString = "UpdateOrderItems";
				SqlCommand command = new SqlCommand(commandString, mConnection);
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.AddWithValue("@ItemId", order.ItemId);
				command.Parameters.AddWithValue("@OrderId", OrderId);
				command.Parameters.AddWithValue("@NewQuantity", order.Quantity);
				command.Parameters.AddWithValue("@IsActive", order.isActive);

				// Execute the command.
				mConnection.Open();
				command.ExecuteNonQuery();

				// Close the connection.
				mConnection.Close();
			}
			catch(Exception ex)
			{
				_log.Error("Error while updating the order item details for the Order id : " + OrderId + ". Details : " + ex.Message);
				throw ex;
			}
		}

		public DataTable ViewOrdersByBuyerIdandOrderId(string BuyerId, string OrderId)
		{
			try
			{
				// Create the SQL connection.
				SqlConnection mConnection = new SqlConnection(_conStrOMS);

				string commandString = "GetOrderDetails";

				SqlParameter[] sqlParams = {
				 new SqlParameter("@BuyerId", string.IsNullOrEmpty(BuyerId) ? "0" : BuyerId),
				 new SqlParameter("@OrderId", string.IsNullOrEmpty(OrderId) ? "0" : OrderId)
				 };

				DataSet dsOrderDetails = SqlHelper.ExecuteDataset(mConnection, commandString, sqlParams);

				if (dsOrderDetails.Tables.Count > 0)
					return dsOrderDetails.Tables[0];
				else
					return null;
			}
			catch(Exception ex)
			{
				_log.Error("Error while getting the order details by the Order id : " + OrderId + " and by the Buyer id : " + BuyerId + ". Details : " + ex.Message);
				throw ex;
			}
		}

		public DataTable GetOrdersByOrderId(string OrderId)
		{
			try
			{
				// Create the SQL connection.
				SqlConnection mConnection = new SqlConnection(_conStrOMS);

				string commandString = "GetOrderItemsByOrderId";

				SqlParameter[] sqlParams = {
				 new SqlParameter("@OrderId", OrderId)
				 };

				DataSet dsOrderDetails = SqlHelper.ExecuteDataset(mConnection, commandString, sqlParams);

				if (dsOrderDetails.Tables.Count > 0)
					return dsOrderDetails.Tables[0];
				else
					return null;
			}
			catch(Exception ex)
			{
				_log.Error("Error while getting the order item details for the Order id : " + OrderId + ". Details : " + ex.Message);
				throw ex;
			}
		}

	}
}