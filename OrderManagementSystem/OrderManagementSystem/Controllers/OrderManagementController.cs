﻿// Created By :   Mahesh Kandi
// Created Date : 30th Dec, 2020
// Description :  This file contains the implementations of the Order Management System API's.
// ==============================================================================


using log4net;
using Newtonsoft.Json;
using OrderManagementSystem.Models;
using OrderManagementSystem.SQLDataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;

namespace OrderManagementSystem.Controllers
{
	[RoutePrefix("api/OrderManagement")]
    public class OrderManagementController : ApiController
    {
		ILog _log = log4net.LogManager.GetLogger(typeof(OrderManagementController));
		DataAccess _dataAccess = new DataAccess();

		[Route("InsertOrderDetails"), HttpPost]
		public HttpResponseMessage InsertOrderDetails(OrderDetails orderDetails)
		{
			try
			{
				HttpResponseMessage apiResponse = null;

				if (orderDetails == null)
				{
					_log.Info("Invalid Order. Please check.");
					apiResponse = Request.CreateResponse(HttpStatusCode.OK, "Invalid Order. Please check.");
				}
				else if (orderDetails.Orders == null || orderDetails.Orders.Count == 0)
				{
					_log.Info("Order details not found. Please check.");
					apiResponse = Request.CreateResponse(HttpStatusCode.OK, "Order details not found. Please check.");
				}
				else
				{
					int orderId = 0;

					_log.Info("Started inserting the Order details");

					using (TransactionScope transaction = new TransactionScope())
					{
						//Insert Order Details
						orderId = _dataAccess.InsertOrderDetails(orderDetails.BuyerId, orderDetails.AddressId, orderDetails.OrderStatus);

						//Insert Order Items
						foreach (OrderItems order in orderDetails.Orders)
						{
							_dataAccess.InsertOrderItemDetails(orderId, order.ProductId, order.Quantity);
						}

						transaction.Complete();

						_log.Info("Order details inserted successdully. Order Id is: " + orderId);

					}
					apiResponse = Request.CreateResponse(HttpStatusCode.OK, "Order Placed Successfully. Your Order Id Is: " + Convert.ToString(orderId));										
				}

				return apiResponse;
			}
			catch (Exception ex)
			{
				_log.Error("Failed to insert the Order details" + ex.Message);
				return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
			}
		}

		[Route("UpdateOrderDetails"), HttpPut]
		public HttpResponseMessage UpdateOrderDetails(OrderDetails orderDetails)
		{
			int orderId = 0;
			try
			{
				HttpResponseMessage apiResponse = null;

				if (orderDetails == null || orderDetails.OrderId <= 0)
				{
					_log.Info("Invalid Order. Please check.");
					apiResponse = Request.CreateResponse(HttpStatusCode.OK, "Invalid Order. Please check.");
				}
				else if (orderDetails.Orders == null || orderDetails.Orders.Count == 0)
				{
					_log.Info("Order details not found. Please check.");
					apiResponse = Request.CreateResponse(HttpStatusCode.OK, "Order details not found. Please check.");
				}
				else
				{
					orderId = orderDetails.OrderId;
					_log.Info("Started updating the Order details for the Order Id: " + orderDetails.OrderId);

					using (TransactionScope transaction = new TransactionScope())
					{
						//Update Order Details
						_dataAccess.UpdateOrderStatusByOrderId(orderDetails.OrderId, orderDetails.OrderStatus);

						//Update Order Items
						if (orderDetails.OrderStatus != 3)			//When order is cancelled - We are handling while updating the Order details table it self.
						{
							foreach (OrderItems order in orderDetails.Orders)
							{

								if (order.ItemId > 0)
									_dataAccess.UpdateOrderItemDetails(order, orderDetails.OrderId);                        //Update Order
								else
									_dataAccess.InsertOrderItemDetails(orderDetails.OrderId, order.ProductId, order.Quantity);      //Insert New Order
							}
						}

						transaction.Complete();
						_log.Info("Order Details Updated Successfully.");

					}
					apiResponse = Request.CreateResponse(HttpStatusCode.OK, "Order Details Updated Successfully.");
				}

				return apiResponse;
			}
			catch (Exception ex)
			{
				_log.Info("Failed to update the Order details for the Order Id: " + orderId + ". Details: " + ex.Message);
				return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
			}
		}

		[Route("UpdateOrderStatusByOrderId"), HttpPut]
		public HttpResponseMessage UpdateOrderStatusByOrderId(OrderUpdate order)
		{
			if (_dataAccess.UpdateOrderStatusByOrderId(order.OrderId, order.OrderStatus))
			{
				_log.Info("Order status upadted successfully");
				return Request.CreateResponse(HttpStatusCode.OK, "Order status upadted successfully");
			}
			
			_log.Info("Failed to update the Order status");
			return Request.CreateResponse(HttpStatusCode.OK, "Failed to update the Order status");
		}

		[Route("GetOrderDetails/{BuyerId?}/{OrderId?}"), HttpGet]
		public HttpResponseMessage GetOrderDetails(string BuyerId = null, string OrderId = null)
		{
			try
			{
				_log.Info("Getting the Order details for the Buyer Id: " + BuyerId + " Order Id: " + OrderId);
				HttpResponseMessage apiResponse = null;

				//Get All Orders By OrderId and BuyerId
				DataTable dtOrderDetails = _dataAccess.ViewOrdersByBuyerIdandOrderId(BuyerId, OrderId);

				List<OrderDetails> listOrderDetails = new List<OrderDetails>();

				if(dtOrderDetails != null && dtOrderDetails.Rows.Count > 0)
				{
					OrderDetails ordDetails = null;

					foreach (DataRow drOrder in dtOrderDetails.Rows)
					{
						ordDetails = new OrderDetails();

						ordDetails.OrderId = Convert.ToInt32(drOrder["OrderId"]);
						ordDetails.BuyerId = Convert.ToInt32(drOrder["BuyerId"]);
						ordDetails.BuyerName = Convert.ToString(drOrder["BuyerName"]);
						ordDetails.PrimaryContactNo = Convert.ToString(drOrder["PrimaryContactNo"]);
						ordDetails.AddressId = Convert.ToInt32(drOrder["AddressId"]);
						ordDetails.DeliveryAddress = Convert.ToString(drOrder["DeliveryAddress"]);
						ordDetails.OrderStatusDesc = Convert.ToString(drOrder["Status"]);
						ordDetails.OrderPlacedOn = Convert.ToString(drOrder["OrderPlacedOn"]);

						DataTable dtOrderItems = _dataAccess.GetOrdersByOrderId(Convert.ToString(drOrder["OrderId"]));
						OrderItems itemDetails;

						//Get all product items for each order
						foreach (DataRow drItem in dtOrderItems.Rows)
						{
							itemDetails = new OrderItems();
							itemDetails.ProductId = Convert.ToInt32(drItem["ProductId"]);
							itemDetails.ProductName = Convert.ToString(drItem["Name"]);
							itemDetails.Quantity = Convert.ToInt32(drItem["Quantity"]);

							ordDetails.Orders.Add(itemDetails);
						}

						listOrderDetails.Add(ordDetails);
					}
				}

				apiResponse = Request.CreateResponse(HttpStatusCode.OK, JsonConvert.SerializeObject(listOrderDetails));
				
				return apiResponse;
			}
			catch (Exception ex)
			{
				_log.Error("Error while getting the order details. Details: " + ex.Message);
				return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
			}
		}
	}
}